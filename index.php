<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <style>
    * {
      padding: 0px;
      margin: 0px;
      font-weight: 400;
    }
  </style>
</head>
<body>

</body>
</html>

<?php

/*
PHP Global Variables - Superglobals
Some predefined variables in PHP are "superglobals",
which means that they are always accessible,
regardless of scope - and you can access them from any function,
class or file without having to do anything special.

The PHP superglobal variables are:
*/

/*
$GLOBALS
  - $GLOBALS is a special PHP superglobal variable that contains a reference
    to all variables that are currently defined in the global scope of the script.
    This means that any variable that is defined outside of a function
    or method can be accessed and modified using the $GLOBALS array.
*/

echo "<hr>";

echo '<h2>$GLOBALS</h2>';

$x = 75;
$y = 25;

function addition() {
  echo $GLOBALS["x"] + $GLOBALS["y"];
}

addition();

echo "<hr>";

echo '<h2>$_SERVER</h2>';

/*
$_SERVER
  - $_SERVER is another PHP superglobal variable that contains an array
    of information related to the current HTTP request and the environment
    in which the PHP script is running. It includes information such as the
    request method, the requested URL, the user agent, and many other details.
*/
print_r($_SERVER);

echo "<hr>";

echo '<h2>$_REQUEST</h2>';

/*
$_REQUEST
  - $_REQUEST is a PHP superglobal variable that contains the values of the
    $_GET, $_POST and $_COOKIE superglobal variables. In other words,
    it combines the data that is sent to a PHP script through the URL (using the GET method),
    through an HTML form (using the POST method), and through cookies.

  - The $_REQUEST array is created automatically by PHP when a script is executed,
    and it is populated with the values from the different superglobal arrays
    based on the method used to make the request. For example, if a form is submitted
    using the POST method, the values of the form fields will be available in
    $_REQUEST as well as in $_POST.
*/
print_r($_REQUEST);

echo "<hr>";

echo '<h2>$_POST</h2>';

/*
$_POST
  - $_POST is a PHP superglobal variable that contains an array of data
    that was submitted to a PHP script using the HTTP POST method.
    When a form is submitted using the POST method, the values of
    the form fields are included in the $_POST array.
*/
print_r($_POST);

/*
$_GET
*/
echo "<hr>";

echo '<h2>$_GET</h2>';

print_r($_GET);

/*
$_SESSION
  - The session support allows you to store data between requests in the $_SESSION superglobal array. When a visitor accesses your site, PHP will check automatically (if session.auto_start is set to 1) or on your request (explicitly through session_start()) whether a specific session id has been sent with the request. If this is the case, the prior saved environment is recreated.
*/
echo "<hr>";

echo '<h2>$_SESSION</h2>';

session_start();
print_r($_SESSION);

/*
$_FILES
  - An associative array of items uploaded to the current script via the HTTP POST method. The structure of this array is outlined in the POST method uploads section.
*/
echo "<hr>";

echo '<h2>$_FILES</h2>';

print_r($_FILES);

/*
$_ENV
  - An associative array of variables passed to the current script via the environment method.

  - These variables are imported into PHP's global namespace from the environment under which the PHP parser is running. Many are provided by the shell under which PHP is running and different systems are likely running different kinds of shells, a definitive list is impossible. Please see your shell's documentation for a list of defined environment variables.
*/
echo "<hr>";

echo '<h2>$_ENV</h2>';

print_r($_ENV);

/*
$_COOKIE
  - An associative array of variables passed to the current script via HTTP Cookies.
*/

echo "<hr>";

echo '<h2>$_COOKIE</h2>';

print_r($_COOKIE);